package com.ToDo.List.controller;

import com.ToDo.List.Dto.ToDoListDto;
import com.ToDo.List.Model.ToDoList;
import com.ToDo.List.Response.CommonResponse;
import com.ToDo.List.Response.ResponHelper;
import com.ToDo.List.Service.ToDoListService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/list")
@CrossOrigin(origins = "http://localhost:3000")
public class ToDoListController {

    @Autowired
    ToDoListService toDoListService;

    @Autowired
    ModelMapper modelMapper;

    @GetMapping("/{id}")
    public CommonResponse<ToDoList> getListById(@PathVariable("id") Long id) {
        return ResponHelper.ok(toDoListService.getListById(id));
    }
    @GetMapping
    public CommonResponse<List<ToDoList>> findAccount(Long userId) {
        return ResponHelper.ok(toDoListService.findAccount(userId));
    }
    @PostMapping
    public CommonResponse<ToDoList> addList(@RequestBody ToDoListDto toDoListDto) {
        return ResponHelper.ok(toDoListService.addList(toDoListDto));
    }
    @PutMapping("/{id}")
    public CommonResponse<ToDoList> editList(@PathVariable("id") Long id, @RequestBody ToDoListDto toDoListDto) {
        return ResponHelper.ok(toDoListService.editList(id, toDoListDto));
    }
    @DeleteMapping("/{id}")
    public CommonResponse<?> deleteList(@PathVariable("id") Long id) {
        return ResponHelper.ok(toDoListService.deleteList(id));
    }
}
