package com.ToDo.List.controller;

import com.ToDo.List.Dto.PresensiDto;
import com.ToDo.List.Dto.ToDoListDto;
import com.ToDo.List.Model.Presensi;
import com.ToDo.List.Model.ToDoList;
import com.ToDo.List.Response.CommonResponse;
import com.ToDo.List.Response.ResponHelper;
import com.ToDo.List.Service.PresensiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/presensi")
@CrossOrigin(origins = "http://localhost:3000")
public class PresensiController {

    @Autowired
    private PresensiService presensiService;

    @GetMapping
    public CommonResponse<Page<Presensi>> findUser(@RequestParam Long userId, int page) {
        return ResponHelper.ok(presensiService.findUser(userId, page));
    }

    @PostMapping("/absen-masuk")
    public CommonResponse<Presensi> absenMasuk(@RequestBody PresensiDto presensiDto) {
        return ResponHelper.ok(presensiService.absenMasuk(presensiDto));
    }

    @PostMapping("/absen-pulang")
    public CommonResponse<Presensi> absenPulang(@RequestBody PresensiDto presensiDto) {
        return ResponHelper.ok(presensiService.absenPulang(presensiDto));
    }


    @DeleteMapping("/{id}")
    public CommonResponse<?> deletePresensi(@PathVariable("id") Long id) {
        return ResponHelper.ok(presensiService.deletePresensi(id));
    }
}
