package com.ToDo.List.controller;

import com.ToDo.List.Dto.HistoryDto;
import com.ToDo.List.Dto.PresensiDto;
import com.ToDo.List.Model.Presensi;
import com.ToDo.List.Model.TableTask;
import com.ToDo.List.Response.CommonResponse;
import com.ToDo.List.Response.ResponHelper;
import com.ToDo.List.Service.TableTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/task")
@CrossOrigin(origins = "http://localhost:3000")
public class TableTaskController {

    @Autowired
    TableTaskService tableTaskService;

    @PostMapping("/selesai")
    public CommonResponse<TableTask> addHistory(@RequestBody HistoryDto historyDto) {
        return ResponHelper.ok(tableTaskService.addHistory(historyDto));
    }
    @GetMapping
    public CommonResponse<Page<TableTask>> findTask(@RequestParam Long page, Long userId) {
        return ResponHelper.ok(tableTaskService.findTask(userId, page));
    }
    @DeleteMapping("/{id}")
    public CommonResponse<?> deleteHistory(@PathVariable("id") Long id) {
        return ResponHelper.ok(tableTaskService.deleteHistory(id));
    }
    @DeleteMapping
    public void deleteAll(@RequestParam int userId) {
        tableTaskService.deleteAll(userId);
    }
}
