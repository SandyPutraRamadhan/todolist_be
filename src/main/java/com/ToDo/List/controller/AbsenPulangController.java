package com.ToDo.List.controller;

import com.ToDo.List.Dto.AbsenPulangDto;
import com.ToDo.List.Model.AbsenPulang;
import com.ToDo.List.Model.Presensi;
import com.ToDo.List.Response.CommonResponse;
import com.ToDo.List.Response.ResponHelper;
import com.ToDo.List.Service.AbsenPulangService;
import com.ToDo.List.Service.PresensiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/pulang")
@CrossOrigin(origins = "http://localhost:3000")
public class AbsenPulangController {

    @Autowired
    private AbsenPulangService absenPulangService;

    @PostMapping
    public CommonResponse<AbsenPulang> absen(@RequestBody AbsenPulangDto absenPulangDTO) {
        return ResponHelper.ok(absenPulangService.absen(absenPulangDTO));
    }
    @GetMapping
    public CommonResponse<Page<AbsenPulang>> findUser(@RequestParam Long userId, int page) {
        return ResponHelper.ok(absenPulangService.findUser(userId, page));
    }

    @DeleteMapping("/{id}")
    public CommonResponse<?>  delete(@PathVariable("id") Long id) {
        return ResponHelper.ok(absenPulangService.deleteById(id));
    }

}
