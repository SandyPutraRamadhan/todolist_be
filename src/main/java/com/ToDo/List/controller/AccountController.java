package com.ToDo.List.controller;

import com.ToDo.List.Dto.*;
import com.ToDo.List.Model.Account;
import com.ToDo.List.Response.CommonResponse;
import com.ToDo.List.Response.ResponHelper;
import com.ToDo.List.Service.AccountService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/users")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping(path = "/{id}")
    public CommonResponse<Account> getAccountById(@PathVariable("id") Long id) {
        return ResponHelper.ok(accountService.getAccountById(id));
    }

    @GetMapping
    public CommonResponse<List<Account>> getAllAccount() {
        return ResponHelper.ok(accountService.getAllAccount());
    }
    @PostMapping("/sign-in")
    public CommonResponse<Map<String, Object>> login (@RequestBody LoginDto loginDto) {
        return ResponHelper.ok(accountService.login(loginDto));
    }
    @PostMapping(path = "/sign-up")
    public CommonResponse<Account> addAccount(@RequestBody Account account) {
        return ResponHelper.ok(accountService.addAccount(account));
    }
    @PutMapping(path = "/{id}", consumes = "multipart/form-data")
    public CommonResponse<Account> editAccount(@PathVariable("id") Long id , EditProfileDto editProfileDto, @RequestPart("file") MultipartFile multipartFile) {
        return ResponHelper.ok(accountService.editAccount(id, modelMapper.map(editProfileDto, Account.class), multipartFile));
    }
    @PutMapping(path = "/editbg/{id}", consumes = "multipart/form-data")
    public CommonResponse<Account> editBackground(@PathVariable("id") Long id , @RequestPart("file") MultipartFile multipartFile) {
        return ResponHelper.ok(accountService.editBackground(id, multipartFile));
    }
    @PutMapping(path = "/editpp/{id}", consumes = "multipart/form-data")
    public CommonResponse<Account> editPP(@PathVariable("id") Long id , @RequestPart("file") MultipartFile multipartFile) {
        return ResponHelper.ok(accountService.editPP(id, multipartFile));
    }
    @PutMapping(path = "/editpw/{id}")
    public CommonResponse<Account> editPassword(@PathVariable("id") Long id , @RequestBody PasswordDto passwordDto) {
        return ResponHelper.ok(accountService.editPassword(id, passwordDto));
    }
    @DeleteMapping(path = "/{id}")
    public CommonResponse<?> deleteAccount(@PathVariable("id") Long id) {
        return ResponHelper.ok(accountService.deleteAccount(id));
    }
}
