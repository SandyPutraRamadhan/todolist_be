package com.ToDo.List.Service;

import com.ToDo.List.Dto.HistoryDto;
import com.ToDo.List.Model.TableTask;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface TableTaskService {

    TableTask addHistory(HistoryDto historyDto);

    Page<TableTask> findTask(Long userId, Long page);

    Map<String, Boolean> deleteHistory(Long id);

    void deleteAll(int userId);
}
