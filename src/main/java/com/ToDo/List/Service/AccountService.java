package com.ToDo.List.Service;

import com.ToDo.List.Dto.BackgroundDto;
import com.ToDo.List.Dto.LoginDto;
import com.ToDo.List.Dto.PasswordDto;
import com.ToDo.List.Model.Account;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface AccountService {

    Map<String, Object> login(LoginDto loginDto);

    Account getAccountById(Long id);

    Account addAccount(Account account );

    List<Account> getAllAccount();

    Account editAccount(Long id , Account account, MultipartFile multipartFile);

    Account editBackground(Long id, MultipartFile multipartFile);

    Account editPassword(Long id, PasswordDto passwordDto);

    Account editPP(Long id, MultipartFile multipartFile);

    Map<String, Boolean> deleteAccount(Long id);
}
