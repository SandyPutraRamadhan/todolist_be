package com.ToDo.List.Service;

import com.ToDo.List.Dto.ToDoListDto;
import com.ToDo.List.Model.Account;
import com.ToDo.List.Model.ToDoList;

import java.util.List;
import java.util.Map;

public interface ToDoListService {

    ToDoList getListById(Long id);

    ToDoList addList(ToDoListDto toDoListDto);

    List<ToDoList> findAccount(Long userId);

    ToDoList editList(Long id , ToDoListDto toDoListDto);

    Map<String, Boolean> deleteList(Long id);
}
