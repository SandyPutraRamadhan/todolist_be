package com.ToDo.List.Service;

import com.ToDo.List.Dto.AbsenPulangDto;
import com.ToDo.List.Dto.PresensiDto;
import com.ToDo.List.Model.AbsenPulang;
import com.ToDo.List.Model.Presensi;
import org.springframework.data.domain.Page;

import java.util.Map;

public interface AbsenPulangService {

    AbsenPulang absen(AbsenPulangDto absenPulangDto);

    Page<AbsenPulang> findUser(Long userId, int page);


    Map<String, Boolean> delete(Long id);

    Map<String, Boolean> deleteById(Long id);
}
