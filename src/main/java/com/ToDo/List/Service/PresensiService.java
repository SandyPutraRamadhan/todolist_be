package com.ToDo.List.Service;

import com.ToDo.List.Dto.PresensiDto;
import com.ToDo.List.Model.Presensi;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface PresensiService {

    Presensi absenMasuk(PresensiDto presensiDto);
    Presensi absenPulang(PresensiDto presensiDto);

    Page<Presensi> findUser(Long userId, int page);


    Map<String, Boolean> deletePresensi(Long id);
    Map<String, Boolean> deleteAbsen(Long id);
}
