package com.ToDo.List.Dto;

import com.ToDo.List.Model.AccountEnum;

public class RegistrasiDto {

    private String nama;

    private String email;

    private String password;

    private String alamat;

    private String telepon;

    private AccountEnum role;

    public AccountEnum getRole() {
        return role;
    }

    public void setRole(AccountEnum role) {
        this.role = role;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getTelepon() {
        return telepon;
    }

    public void setTelepon(String telepon) {
        this.telepon = telepon;
    }
}
