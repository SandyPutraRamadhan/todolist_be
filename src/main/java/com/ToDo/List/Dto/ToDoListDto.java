package com.ToDo.List.Dto;

public class ToDoListDto {

    private Long userId;

    private String tugas;


    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getTugas() {
        return tugas;
    }

    public void setTugas(String tugas) {
        this.tugas = tugas;
    }

}
