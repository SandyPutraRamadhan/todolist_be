package com.ToDo.List.Dto;

import com.ToDo.List.Model.PresensiEnum;

public class PresensiDto {

    private Long userId;

    private PresensiEnum role;

    public PresensiEnum getRole() {
        return role;
    }

    public void setRole(PresensiEnum role) {
        this.role = role;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

}
