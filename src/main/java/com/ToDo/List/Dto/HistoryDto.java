package com.ToDo.List.Dto;

import com.ToDo.List.Model.Account;
import com.ToDo.List.Model.TableTaskEnum;
import com.ToDo.List.Model.ToDoList;

public class HistoryDto {

    private Long toDoList;

    private Long userId;

    private TableTaskEnum role;

    public Long getToDoList() {
        return toDoList;
    }

    public void setToDoList(Long toDoList) {
        this.toDoList = toDoList;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public TableTaskEnum getRole() {
        return role;
    }

    public void setRole(TableTaskEnum role) {
        this.role = role;
    }
}
