package com.ToDo.List.Dto;

import com.ToDo.List.Model.AccountEnum;

public class BackgroundDto {

    private String background;

    private AccountEnum role;

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public AccountEnum getRole() {
        return role;
    }

    public void setRole(AccountEnum role) {
        this.role = role;
    }
}
