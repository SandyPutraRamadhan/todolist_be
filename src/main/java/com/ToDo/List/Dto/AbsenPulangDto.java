package com.ToDo.List.Dto;

import com.ToDo.List.Model.Account;
import com.ToDo.List.Model.PresensiEnum;

import java.util.Date;

public class AbsenPulangDto {

    private Long userId;

    private PresensiEnum role;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public PresensiEnum getRole() {
        return role;
    }

    public void setRole(PresensiEnum role) {
        this.role = role;
    }
}
