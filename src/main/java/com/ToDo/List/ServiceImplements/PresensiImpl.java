package com.ToDo.List.ServiceImplements;

import com.ToDo.List.Dto.PresensiDto;
import com.ToDo.List.Exeption.InternalErrorException;
import com.ToDo.List.Exeption.NotFoundException;
import com.ToDo.List.Model.Presensi;
import com.ToDo.List.Model.PresensiEnum;
import com.ToDo.List.Model.ToDoList;
import com.ToDo.List.Repository.AccountRepository;
import com.ToDo.List.Repository.PresensiRepository;
import com.ToDo.List.Service.PresensiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PresensiImpl implements PresensiService {

    private static final Integer hour = 3600 * 1000;

    @Autowired
     AccountRepository accountRepository;

    @Autowired
     PresensiRepository presensiRepository;


    @Override
    public Presensi absenMasuk(PresensiDto presensiDto) {
        Presensi presensi = new Presensi();
        presensi.setJamMulai(new Date(new Date().getTime() + 7 * hour));
        presensi.setUserId(accountRepository.findById(presensiDto.getUserId()).orElseThrow(() -> new NotFoundException("Id not found")));
        presensi.setRole(PresensiEnum.MASUK);
        return presensiRepository.save(presensi);
    }

    @Override
    public Presensi absenPulang(PresensiDto presensiDto) {
        Presensi presensi = new Presensi();
        presensi.setJamMulai(new Date(new Date().getTime() + 7 * hour));
        presensi.setUserId(accountRepository.findById(presensiDto.getUserId()).orElseThrow(() -> new NotFoundException("Id not found")));
        presensi.setRole(PresensiEnum.PULANG);
        return presensiRepository.save(presensi);
    }

    @Override
    public Page<Presensi> findUser(Long userId, int page) {
        Pageable pageable = PageRequest.of(page, 5);
        return presensiRepository.findUser(userId, pageable);
    }



    @Override
    public Map<String, Boolean> deletePresensi(Long id) {
        try {
            presensiRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }

    @Override
    public Map<String, Boolean> deleteAbsen(Long id) {
        try {
            presensiRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }
}
