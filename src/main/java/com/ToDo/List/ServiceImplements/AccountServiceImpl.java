package com.ToDo.List.ServiceImplements;

import com.ToDo.List.Dto.BackgroundDto;
import com.ToDo.List.Dto.LoginDto;
import com.ToDo.List.Dto.PasswordDto;
import com.ToDo.List.Exeption.InternalErrorException;
import com.ToDo.List.Exeption.NotFoundException;
import com.ToDo.List.Jwt.JwtProvider;
import com.ToDo.List.Model.Account;
import com.ToDo.List.Repository.AccountRepository;
import com.ToDo.List.Service.AccountService;
import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    UsersDetailsImpl userDetailsService;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtProvider jwtProvider;

    //    FireBase
    private static final String DOWNLOAD_URL = "https://firebasestorage.googleapis.com/v0/b/image-react-eeb49.appspot.com/o/%s?alt=media";

    private String imageConverter(MultipartFile multipartFile) {
        try {
            String fileName = getExtention(multipartFile.getOriginalFilename());
            File file = convertToFile(multipartFile, fileName);
            var RESPONSE_URL = uploadFile(file, fileName);
            file.delete();
            return RESPONSE_URL;
        } catch (Exception e) {
            e.getStackTrace();
            throw new InternalErrorException("Error Upload File");
        }
    }

    private File convertToFile(MultipartFile multipartFile, String fileName) throws IOException {
        File file = new File(fileName);
        try (FileOutputStream fos = new FileOutputStream(file)) {
            fos.write(multipartFile.getBytes());
            fos.close();
        }
        return file;
    }

    private String uploadFile(File file, String fileName) throws IOException {
        BlobId blobId = BlobId.of("image-react-eeb49.appspot.com", fileName);
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("media").build();
        Credentials credentials = GoogleCredentials.fromStream(new FileInputStream("./src/main/resources/ServiceAccount.json"));
        Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
        storage.create(blobInfo, Files.readAllBytes(file.toPath()));
        return String.format(DOWNLOAD_URL, URLEncoder.encode(fileName, StandardCharsets.UTF_8));
    }

    private String getExtention(String filename) {
        return filename.split("\\.")[0];
    }
//    End FireBase

    //    Base 64
    private String convertToBase64Url(MultipartFile file) {
        String url = "";
        try {
            byte[] byteData = Base64.encodeBase64(file.getBytes());
            String result = new String(byteData);
            url = "data:" + file.getContentType() + ";base64," + result;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return url;
        }

    }

    private String authories(String email, String password) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
        } catch (BadCredentialsException e) {
            throw new InternalErrorException("Email or Password NOT Found");
        }
        UserDetails userDetails = userDetailsService.loadUserByUsername(email);
        return jwtProvider.generateToken(userDetails);
    }


    @Override
    public Map<String, Object> login(LoginDto loginDto) {
        String token = authories(loginDto.getEmail(), loginDto.getPassword());
        Account account = accountRepository.findByEmail(loginDto.getEmail()).orElseThrow(() -> new NotFoundException("Email not found"));
        Map<String, Object> response = new HashMap<>();
        response.put("token", token);
        response.put("expired", "15 menit");
        response.put("user", account);
        return response;
    }

    @Override
    public Account getAccountById(Long id) {
        var respon = accountRepository.findById(id).orElseThrow(() -> new NotFoundException("Id Not Found"));
        try {
            return accountRepository.save(respon);
        } catch (Exception e) {
            throw new InternalErrorException("Kesalahan Memunculkan Data");
        }
    }

    @Override
    public Account addAccount(Account account) {
//        String foto = imageConverter(multipartFile);
//        Account addAccount = new Account(account.getNama(), account.getEmail(), account.getPassword(),account.getAlamat(), foto, account.getTelepon(), account.getRole());
        String email = account.getEmail();
        var validasi = accountRepository.findByEmail(email);
        if (validasi.isPresent()) {
            throw new InternalErrorException("Email Already Axist");
        }
        account.setPassword(passwordEncoder.encode(account.getPassword()));
        return accountRepository.save(account);
    }
 
    @Override
    public List<Account> getAllAccount() {
        return accountRepository.findAll();
    }

    @Override
    public Account editAccount(Long id, Account account, MultipartFile multipartFile) {
        Account update = accountRepository.findById(id).orElseThrow(() -> new NotFoundException("Account Not Found"));
        update.setNama(account.getNama());
        update.setTelepon(account.getTelepon());
        update.setAlamat(account.getAlamat());
        return accountRepository.save(update);
    }

    @Override
    public Account editBackground(Long id, MultipartFile multipartFile) {
        Account edit = accountRepository.findById(id).orElseThrow(() -> new NotFoundException("USER ID NOT FOUND"));
        String url = convertToBase64Url(multipartFile);
        edit.setBackground(url);
        return accountRepository.save(edit);
    }

    @Override
    public Account editPassword(Long id, PasswordDto passwordDto) {
        Account update = accountRepository.findById(id).orElseThrow(() -> new NotFoundException("Password Not Found"));
        update.setPassword(passwordDto.getPassword());
        update.setPassword(passwordEncoder.encode(passwordDto.getPassword()));
        return accountRepository.save(update);
    }

    @Override
    public Account editPP(Long id, MultipartFile multipartFile) {
        Account pp = accountRepository.findById(id).orElseThrow(() -> new NotFoundException("USER ID NOT FOUND"));
        String url = convertToBase64Url(multipartFile);
        pp.setFoto(url);
        return accountRepository.save(pp);
    }

    @Override
    public Map<String, Boolean> deleteAccount(Long id) {
        try {
            accountRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }
}
