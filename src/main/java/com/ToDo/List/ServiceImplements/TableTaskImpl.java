package com.ToDo.List.ServiceImplements;

import com.ToDo.List.Dto.HistoryDto;
import com.ToDo.List.Exeption.NotFoundException;
import com.ToDo.List.Model.TableTask;
import com.ToDo.List.Model.TableTaskEnum;
import com.ToDo.List.Model.ToDoList;
import com.ToDo.List.Repository.AccountRepository;
import com.ToDo.List.Repository.TableTaskRepository;
import com.ToDo.List.Repository.ToDoListRepository;
import com.ToDo.List.Service.TableTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class TableTaskImpl implements TableTaskService {

    @Autowired
    TableTaskRepository tableTaskRepository;

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    ToDoListRepository toDoListRepository;


    @Override
    public TableTask addHistory(HistoryDto historyDto) {
        TableTask create = new TableTask();
        create.setAccount(accountRepository.findById(historyDto.getUserId()).orElseThrow(() -> new NotFoundException("User id tidak di temukan")));
        create.setRole(TableTaskEnum.SELESAI);
        create.setToDoList(toDoListRepository.findById(historyDto.getToDoList()).orElseThrow(() -> new NotFoundException("Id Not Found!")));
        return tableTaskRepository.save(create);
    }

    @Override
    public Page<TableTask> findTask(Long userId, Long page) {
        Pageable pageable = PageRequest.of(Math.toIntExact(page), 10);
        return tableTaskRepository.findTask(userId, pageable);
    }

    @Override
    public Map<String, Boolean> deleteHistory(Long id) {
        try {
            tableTaskRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }

    @Override
    public void deleteAll(int userId) {
        var data = tableTaskRepository.deleteAll(userId);
        tableTaskRepository.deleteAll(data);
    }
}
