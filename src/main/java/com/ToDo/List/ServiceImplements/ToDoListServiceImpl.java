package com.ToDo.List.ServiceImplements;

import com.ToDo.List.Dto.ToDoListDto;
import com.ToDo.List.Exeption.InternalErrorException;
import com.ToDo.List.Exeption.NotFoundException;
import com.ToDo.List.Model.ToDoList;
import com.ToDo.List.Repository.AccountRepository;
import com.ToDo.List.Repository.ToDoListRepository;
import com.ToDo.List.Service.ToDoListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ToDoListServiceImpl implements ToDoListService {

    private static final Integer hour = 3600 * 1000;

    @Autowired
    private ToDoListRepository toDoListRepository;

    @Autowired
    AccountRepository accountRepository;

    @Override
    public ToDoList getListById(Long id) {
        var respon = toDoListRepository.findById(id).orElseThrow(() -> new NotFoundException("Id Not Found"));
        try {
            return toDoListRepository.save(respon);
        } catch (Exception e) {
            throw new InternalErrorException("Kesalahan Memunculkan Data");
        }
    }

    @Override
    public ToDoList addList(ToDoListDto toDoListDto) {
        ToDoList toDoList1 = new ToDoList();
        toDoList1.setTugas(toDoListDto.getTugas());
        toDoList1.setTanggalDibuat(new Date(new Date().getTime() + 7 * hour));
        toDoList1.setAccountId(accountRepository.findById(toDoListDto.getUserId()).orElseThrow(() -> new NotFoundException("Not Found")));
        return toDoListRepository.save(toDoList1);
    }

    @Override
    public List<ToDoList> findAccount(Long userId) {
        return toDoListRepository.findAccount(userId);
    }

    @Override
    public ToDoList editList(Long id, ToDoListDto toDoListDto) {
        ToDoList edit = toDoListRepository.findById(id).orElseThrow(() -> new NotFoundException("Id Not Found"));
        edit.setTugas(toDoListDto.getTugas());
        edit.setTanggalDibuat(new Date(new Date().getTime() + 7 * hour));
        edit.setAccountId(accountRepository.findById(toDoListDto.getUserId()).orElseThrow(() -> new NotFoundException(" Not Found")));
        return toDoListRepository.save(edit);
    }

    @Override
    public Map<String, Boolean> deleteList(Long id) {
        try {
            toDoListRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }
    }
