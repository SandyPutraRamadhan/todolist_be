package com.ToDo.List.ServiceImplements;

import com.ToDo.List.Dto.AbsenPulangDto;
import com.ToDo.List.Exeption.NotFoundException;
import com.ToDo.List.Model.AbsenPulang;
import com.ToDo.List.Repository.AbsenPulangRepository;
import com.ToDo.List.Repository.AccountRepository;
import com.ToDo.List.Service.AbsenPulangService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class AbsenPulangImpl implements AbsenPulangService {

    private static final int HOUR = 3600 * 1000;

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    AbsenPulangRepository absenPulangRepository;


    @Override
    public AbsenPulang absen(AbsenPulangDto absenPulangDto) {
        AbsenPulang create = new AbsenPulang();
        create.setJamAkhir(new Date(new Date().getTime() + 7 * HOUR));
        create.setUserId(accountRepository.findById(absenPulangDto.getUserId()).orElseThrow(() -> new NotFoundException("Id not found")));
        return absenPulangRepository.save(create);
    }

    @Override
    public Page<AbsenPulang> findUser(Long userId, int page) {
        Pageable pageable = PageRequest.of(page, 5);
        return absenPulangRepository.findUser(userId, pageable);
    }



    @Override
    public Map<String, Boolean> delete(Long id) {
        try {
            absenPulangRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }

    @Override
    public Map<String, Boolean> deleteById(Long id) {
        try {
            absenPulangRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }
}

