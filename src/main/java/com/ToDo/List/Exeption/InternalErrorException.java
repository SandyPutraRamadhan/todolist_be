package com.ToDo.List.Exeption;


public class InternalErrorException extends RuntimeException{

    public InternalErrorException(String message) {
        super(message);
    }
}
