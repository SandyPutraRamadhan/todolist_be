package com.ToDo.List.Repository;

import com.ToDo.List.Model.AbsenPulang;
import com.ToDo.List.Model.Presensi;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface AbsenPulangRepository extends JpaRepository<AbsenPulang, Long> {

    @Query(value = "SELECT * FROM pulang WHERE user_id = ?1", nativeQuery = true)
    Page<AbsenPulang> findUser(Long userId, Pageable pageable);

}
