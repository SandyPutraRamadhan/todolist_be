package com.ToDo.List.Repository;

import com.ToDo.List.Model.ToDoList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ToDoListRepository extends JpaRepository<ToDoList, Long> {

    @Query(value = "SELECT * FROM list WHERE user_id = ?1", nativeQuery = true)
    List<ToDoList> findAccount(Long userId);

}
