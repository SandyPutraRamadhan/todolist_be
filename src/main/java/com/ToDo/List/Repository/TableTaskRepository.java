package com.ToDo.List.Repository;

import com.ToDo.List.Model.TableTask;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TableTaskRepository extends JpaRepository<TableTask, Long> {

    @Query(value = "SELECT * FROM task WHERE user_id = :userId", nativeQuery = true)
    Page<TableTask> findTask(Long userId, Pageable pageable);

    @Query(value = "SELECT * FROM task WHERE user_id= :userId", nativeQuery = true)
    List<TableTask> deleteAll(int userId);

}
