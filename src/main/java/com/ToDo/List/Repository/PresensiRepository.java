package com.ToDo.List.Repository;

import com.ToDo.List.Model.Presensi;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface PresensiRepository extends JpaRepository<Presensi, Long> {

    @Query(value = "SELECT * FROM presensi WHERE user_id = ?1", nativeQuery = true)
    Page<Presensi> findUser(Long userId, Pageable pageable);
}
