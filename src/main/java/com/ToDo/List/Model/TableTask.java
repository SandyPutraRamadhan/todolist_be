package com.ToDo.List.Model;

import javax.persistence.*;

@Entity
@Table(name = "task")
public class TableTask {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "toDoList_id")
    private ToDoList toDoList;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private Account account;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "task")
    private TableTaskEnum role;

    public TableTask() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }



    public void setToDoList(ToDoList toDoList) {
        this.toDoList = toDoList;
    }

    public ToDoList getToDoList() {
        return toDoList;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public TableTaskEnum getRole() {
        return role;
    }

    public void setRole(TableTaskEnum role) {
        this.role = role;
    }
}
