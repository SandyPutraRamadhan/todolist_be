package com.ToDo.List.Model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "presensi")
public class Presensi {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonFormat(pattern = "HH:mm:ss")
    @Column(name = "jam_mulai", updatable = false)
    private Date JamMulai;

    @CreationTimestamp
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "tanggal_buat", updatable = false)
    private Date tanggalBuat;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private Account userId;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "description")
    private PresensiEnum role;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getJamMulai() {
        return JamMulai;
    }

    public void setJamMulai(Date jamMulai) {
        JamMulai = jamMulai;
    }

    public Account getUserId() {
        return userId;
    }

    public void setUserId(Account userId) {
        this.userId = userId;
    }

    public Date getTanggalBuat() {
        return tanggalBuat;
    }

    public void setTanggalBuat(Date tanggalBuat) {
        this.tanggalBuat = tanggalBuat;
    }

    public PresensiEnum getRole() {
        return role;
    }

    public void setRole(PresensiEnum role) {
        this.role = role;
    }

}
