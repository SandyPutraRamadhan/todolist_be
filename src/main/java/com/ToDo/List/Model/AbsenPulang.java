package com.ToDo.List.Model;


import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "pulang")
public class AbsenPulang {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonFormat(pattern = "HH:mm:ss")
    @Column(name = "jam_akhir", updatable = false)
    private Date jamAkhir;

    @CreationTimestamp
    @JsonFormat(pattern = "dd-MM-yyyy")
    @Column(name = "tanggal_buat", updatable = false)
    private Date tanggalBuat;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private Account userId;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "description")
    private PresensiEnum role;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getJamAkhir() {
        return jamAkhir;
    }

    public void setJamAkhir(Date jamAkhir) {
        this.jamAkhir = jamAkhir;
    }

    public Account getUserId() {
        return userId;
    }

    public void setUserId(Account userId) {
        this.userId = userId;
    }

    public PresensiEnum getRole() {
        return role;
    }

    public void setRole(PresensiEnum role) {
        this.role = role;
    }

    public Date getTanggalBuat() {
        return tanggalBuat;
    }

    public void setTanggalBuat(Date tanggalBuat) {
        this.tanggalBuat = tanggalBuat;
    }
}
