package com.ToDo.List.Model;

import javax.persistence.*;

@Entity
@Table(name = "akun")
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nama")
    private String nama;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "alamat")
    private String alamat;

    @Lob
    @Column(name = "foto")
    private String foto;

    @Lob
    @Column(name = "background")
    private String background;

    @Column(name = "telepon")
    private String telepon;

    @Enumerated(EnumType.STRING)
    @Column(name = "role")
    private AccountEnum role;

    public Account() {
    }

    public Account(String nama, String email , String password, String alamat, String foto, String telepon, AccountEnum role) {
        this.nama = nama;
        this.email = email;
        this.password = password;
        this.alamat = alamat;
        this.foto = foto;
        this.telepon = telepon;
        this.role = role;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public AccountEnum getRole() {
        return role;
    }

    public void setRole(AccountEnum role) {
        this.role = role;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getTelepon() {
        return telepon;
    }

    public void setTelepon(String telepon) {
        this.telepon = telepon;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }
}
